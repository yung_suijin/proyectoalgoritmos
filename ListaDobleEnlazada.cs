using System;
using System.Data.SqlClient;

namespace proyectoalgoritmos
{
    internal class ListaEnlazada
    {
        public Nodo cabeza;
        private string comandoSql, conexion = "Server=DESKTOP-PVJVDLG\\SQLEXPRESS; Database=proyectoAlgoritmos; Trusted_Connection=True";
        private SqlConnection conexionSql;
        private SqlDataReader lector;

        public ListaEnlazada()
        {
            cabeza = null;

            try
            {
                conexionSql = new SqlConnection(conexion);
                conexionSql.Open();
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        public void InsertarDatos()
        {
            Nodo nuevoElemento = new Linea();
            if(cabeza == null)
                cabeza = 
        }

        public void DesplegarLista()
        {
            Nodo p;

            if(cabeza == null)
            {
                Console.WriteLine("La lista esta vacia.");
                return;
            }

            p = cabeza;
            while(p != null)
            {
                Console.WriteLine(p.datos + " ");
                p = p.siguiente;
            }
        }

        public bool Buscar(string estacion)
        {
            int i = 0;
            Nodo p = cabeza;

            while(p != null)
            {
                if(p.datos.estacion == estacion)
                    break;
                i++;
                p = p.siguiente;
            }

            if(p == null)
                Console.WriteLine("No se encuentra");
            else
            {
                Console.WriteLine(estacion + " esta en la posicion: " + i);
                return true;
            }

            return false;
        }
    }
}