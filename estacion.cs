namespace proyectoalgoritmos
{
    internal class estacion
    {
        public int idLinea { get; set; }
        public int idEstacion { get; set; }
        public string nombre { get; set; }
        public int tipo { get; set; }
        public int estatus { get; set; }
        public string puntosInteres { get; set; }

        public estacion next { get; set; }
    }
}