using System.Data.SqlClient;

namespace proyectoalgoritmos
{
    internal class Nodo
    {
        public Linea datos;
        public Nodo siguiente;

        public Nodo(Linea d)
        {
            datos = d;
            siguiente = null;
        }
    }
}