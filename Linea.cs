namespace proyectoalgoritmos
{
    internal class Linea
    {
        public int idLinea { get; set; }
        public int idEstacion { get; set; }
        public string estacion { get; set; }
        public int tipo { get; set; }
        public int estatus { get; set; }
        public string puntosInteres { get; set; }
    }
}