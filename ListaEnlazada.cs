using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace proyectoalgoritmos
{
    internal class TablaDb
    {
        private string comandoSql, conexion = "Server=DESKTOP-PVJVDLG\\SQLEXPRESS; Database=proyectoAlgoritmos; Trusted_Connection=True";
        private SqlConnection conexionSql;
        private SqlDataReader lector;

        public TablaDb()
        {
            try
            {
                conexionSql = new SqlConnection(conexion);
                conexionSql.Open();
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        public List<Linea> Linea1()
        {
            comandoSql = "SELECT [idLinea],[idEstacion],[estacion],[tipo],[estatus],[puntosInteres] FROM [dbo].[linea1]";
            var linea1 = new List<Linea>();

            using(var comando = new SqlCommand(comandoSql, conexionSql))
            {
                using (lector = comando.ExecuteReader())
                {
                    while (lector.Read())
                    {
                        linea1.Add(new Linea //nodo
                        {
                            idLinea = lector.GetInt32(0),
                            idEstacion = lector.GetInt32(1),
                            estacion = lector.GetString(2),
                            tipo = lector.GetInt32(3),
                            estatus = lector.GetInt32(4),
                            puntosInteres = lector.GetString(5)
                        });
                    }
                }
            }

            return linea1;
        }
    }
}